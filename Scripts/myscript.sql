create SCHEMA myElca

create TABLE myElca.Students(
	Camipro_ID INTEGER not NULL,
	Age INTEGER not NULL,
	Name VARCHAR(20) not NULL,
	PRIMARY KEY(Camipro_ID)
);

select * from myElca.Students

insert into myElca.Students values (192685, 28, 'Philippe Heer');

drop TABLE myElca.firstTable