package com.elca.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.elca.objects.Student;

public class StudentDAO {
	private String query;
	private Connection con;
	private PreparedStatement st;

	@SuppressWarnings("deprecation")
	public void connect() {
		try {
			/* localServerDB */
			Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
			con = DriverManager.getConnection("jdbc:derby://localhost:1527/myLocalServerDB;create=true", "Philippe", "1234"); 
			
			/* localFileDB */
//			Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
//			con = DriverManager.getConnection("jdbc:derby:C:\\Users\\phr\\myLocalFileDB;create=true", "Philippe", "1234");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addStudent(Student s) {
		query = "insert into myElca.Students values(?, ?, ?)";
		try {
			st = con.prepareStatement(query);
			st.setInt(1, s.getCamiproID());
			st.setInt(2, s.getAge());
			st.setString(3, s.getName());
			st.executeUpdate();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Student> showStudents() {
		query = "select * from myElca.Students";
		ResultSet rs;
		Statement s;
		ArrayList<Student> students = new ArrayList<Student>();

		try {
			s = con.createStatement();
			rs = s.executeQuery(query);

			while (rs.next()) {
				students.add(new Student(rs.getInt(1), rs.getInt(2), rs.getString(3)));
			}

			return students;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public Student getStudent(int camiproID) {
		query = "select * FROM myElca.Students where camipro_ID =" + 192685;
		ResultSet rs;
		Statement s;

		try {
			s = con.createStatement();
			rs = s.executeQuery(query);

			rs.next();
			return new Student(rs.getInt(1), rs.getInt(2), rs.getString(3));

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
}
