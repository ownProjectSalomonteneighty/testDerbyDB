package com.elca.objects;

public class Student {
	private int camiproID;
	private int age;
	private String name;

	public Student(int camiproID, int age, String name) {
		super();
		this.camiproID = camiproID;
		this.age = age;
		this.name = name;
	}

	public int getCamiproID() {
		return camiproID;
	}

	public void setCamiproID(int camiproID) {
		this.camiproID = camiproID;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Student " + name + " aged " + age + " has caimproID: " + camiproID;
	}
}
