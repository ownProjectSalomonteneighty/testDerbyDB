package com.elca.main;

import java.util.ArrayList;

import com.elca.dao.StudentDAO;
import com.elca.objects.Student;

public class Main {
	public static void main(String[] args) {
		Student s1 = new Student(123458, 20, "Laura");

		StudentDAO dao = new StudentDAO();
		dao.connect();

		dao.addStudent(s1);

		ArrayList<Student> students = dao.showStudents();
		for (Student student : students) {
			System.out.println(student.toString());
		}

		Student s2 = dao.getStudent(192685);
		System.out.println("\n" + s2.toString());

	}
}
